class Meter.Collections.LiveStreamColumnsCollection extends Meter.Collection

  model: Meter.Models.LiveStreamColumnModel

  listen:
    '@ change:sort': 'resetStatuses'

  initialize: (attributes, options) ->
    { @rows, @headers } = options
    unless _.isEmpty @rows
      @addColumnModels()


  addColumnModels: ->
    filters = utils.transposeMatrix @rows
    _.each @headers, (title, id) =>
      @add { id, title, filters: @getFilterObject filters[id] }


  resetStatuses: (changedModel) ->
    @each (model) ->
      if model isnt changedModel
        model.setSilent { sort: 'all' }


  getFilterObject: (values) ->
    res = _.map values, (val) => @processFilter val
    res = @sortFilters res
    res = @replaceEmpty _.uniq res
    _.map res, (value) =>
      formattedValue = @formatFilter value
      { status: 'disable', value, formattedValue }


  processFilter: (filter) ->
    return '[empty]' if @isBadValue filter
    filter.toString().replace /<\/?[^>]+(>|$)/g, ''


  isBadValue: (value) ->
    return true if typeof value is 'object' and _.isEmpty value
    return true if @isEmptyString value


  isEmptyString: (str) ->
    if typeof str is 'string' then return not str.length


  formatFilter: (val) ->
    return utils.formatNum(val) if val and utils.isNumber(val)
    val


  sortFilters: (filters) ->
    return filters.sort() unless utils.isNumber filters[0]
    _.sortBy filters, (f) -> +f


  replaceEmpty: (array) ->
    index = _.indexOf array, '[empty]'
    if index > 0
      first = array[0]
      array[0] = array[index]
      array[index] = first
    array
